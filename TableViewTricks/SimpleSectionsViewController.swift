//
//  SimpleSectionsViewController.swift
//  TableViewTricks
//
//  Created by Jason Khong on 11/26/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import UIKit

class SimpleSectionsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var fruits: [String] = ["Apple", "Guava", "Orange", "Banana", "Kiwi", "Strawberry", "Durian", "Jackfruits"]
    var cars: [String] = ["Toyota", "Ford", "VW", "Proton", "General Motors", "Tesla", "Apple", "Mercedes Benz", "Subaru", "Honda"]
}

extension SimpleSectionsViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return fruits.count
        } else {
            return cars.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell")!

        if indexPath.section == 0 {
            let fruit = fruits[indexPath.row]
            cell.textLabel?.text = fruit
        } else {
            let car = cars[indexPath.row]
            cell.textLabel?.text = car
        }

        return cell
    }
}