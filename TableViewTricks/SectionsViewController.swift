//
//  SectionsViewController.swift
//  TableViewTricks
//
//  Created by Jason Khong on 11/26/15.
//  Copyright © 2015 ApptivityLab. All rights reserved.
//

import UIKit

class SectionsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!

    var letters: [String] = []
    var countries: [String] = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antigua & Deps", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia", "Bosnia Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Rep", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo", "Congo {Democratic Rep}", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland {Republic}", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea North", "Korea South", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar, {Burma}", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russian Federation", "Rwanda", "St Kitts & Nevis", "St Lucia", "Saint Vincent & the Grenadines", "Samoa", "San Marino", "Sao Tome & Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"]
    var countriesByLetter: [String : [String]] = [:]
    
    var selectedCountries: [String] = []
    var refreshControl: UIRefreshControl!

    func sortCountries() {
        for name in self.countries {
            let firstLetter: String = name.substringToIndex(name.startIndex.advancedBy(1))
            if self.countriesByLetter[firstLetter] == nil {
                letters.append(firstLetter)
                self.countriesByLetter[firstLetter] = [name]
            } else {
                self.countriesByLetter[firstLetter]!.append(name)
            }
        }
    }
    
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.sortCountries()
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        /* 8: Refresh */
        self.refreshControl = UIRefreshControl()
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    /* 8: Refresh */
    func refresh() {
        sleep(5)
        self.refreshControl.endRefreshing()
    }
}

extension SectionsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.letters.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let firstLetter = self.letters[section]
        return self.countriesByLetter[firstLetter]!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell")!
        
        let firstLetter = self.letters[indexPath.section]
        let countryName = self.countriesByLetter[firstLetter]![indexPath.row]
        
        cell.textLabel?.text = countryName
        // cell.accessoryType = UITableViewCellAccessoryType.DetailDisclosureButton
        
        /* 7 Multiple Selections */
        if selectedCountries.contains(countryName) {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }

        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let firstLetter = self.letters[section]
        return firstLetter
    }
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        let firstLetter = self.letters[section]
        return "\(countriesByLetter[firstLetter]!.count) Countries Starting with \(firstLetter.capitalizedString)"
    }
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        return self.letters
    }
    
    func tableView(tableView: UITableView, titleForDeleteConfirmationButtonForRowAtIndexPath indexPath: NSIndexPath) -> String? {
        return "Bye!"
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // code
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let firstLetter = self.letters[indexPath.section]
            self.countriesByLetter[firstLetter]!.removeAtIndex(indexPath.row)
            self.tableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        let firstLetter = self.letters[indexPath.section]
        let countryName = self.countriesByLetter[firstLetter]![indexPath.row]

        NSLog("Show some popup for \(countryName)")
    }
    
    /* 7 Multiple Selections */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let firstLetter = self.letters[indexPath.section]
        let countryName = self.countriesByLetter[firstLetter]![indexPath.row]

        if let currentIndex = selectedCountries.indexOf(countryName) {
            selectedCountries.removeAtIndex(currentIndex)
        } else {
            selectedCountries.append(countryName)
        }

        self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
    }
}